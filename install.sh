#!/bin/bash

BASE_PATH="/home/pdxuser"
GALAXY_HOME="$BASE_PATH/galaxy"

cd $BASE_PATH

echo "CLONING GALAXY"
git clone https://github.com/galaxyproject/galaxy.git

echo "Cloning EDIReX tools"
git clone https://gitlab.ics.muni.cz/europdx/pdx-piplines/pdx-analysis-workflows.git

mv "./pdx-analysis-workflows" "./galaxy/tools/pdx-analysis-workflows"

echo "Copying EDIReX tools configuration file into galaxy folder"
cp "$BASE_PATH/edirex-galaxy/tool_conf.xml" "galaxy/config/tool_conf.xml"
cp "$BASE_PATH/edirex-galaxy/galaxy.yml" "galaxy/config/galaxy.yml"

#Postgresql and FTP server setup. Please, change GALAXY_HOME and FTP_UPLOAD_SITE variables
# to your environment, if they are different from default. This script works with
# Galaxy version 19.09.
FTP_UPLOAD_SITE="pipeline.edirex.ics.muni.cz"
./setup_ftp_and_postgresql_galaxy $GALAXY_HOME $FTP_UPLOAD_SITE

echo "Installing rbase and python"
sudo apt-get install r-base python

if [[ $1 == "gossamer" ]]; then
    echo "Installing gossamer tool"
    git clone https://github.com/data61/gossamer.git
    sudo apt-get install g++ cmake libboost-all-dev pandoc zlib1g-dev libbz2-dev libsqlite3-dev -y
    cd gossamer
    mkdir build
    cd build
    cmake ..
    make
    make test
    make install
fi

echo "RUNNING GALAXY NOW"
sudo ./galaxy/run.sh
