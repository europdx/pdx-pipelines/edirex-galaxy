# EDIReX Galaxy

Script for initializing your custom Galaxy instance with EDIReX tools included.

```
chmod u+x install.sh
./install.sh <folder>
```
Parameter `<folder>` - where Galaxy will be cloned from Git. If empty, galaxy will be downloaded to the same folder as `install.sh`.